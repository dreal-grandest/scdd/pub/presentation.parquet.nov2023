# presentation.parquet.nov2023

Ce projet contient le diaporama (format PDF) de la présentation du format de données Apache Parquet, le 8 novembre 2023 à la DREAL Grand Est, par visio. Il inclut quelques exemples de scripts R d'import et de traitement de données sous ce format.  

## Détail des scripts 

**1_tests_rpls_parquet_open.Rmd**,   **2_tests_rpls_parquet_ar.Rmd** et   **3_tests_rpls_parquet_df.Rmd** :  
    3 scripts de mesures de performances sur un jeu de données de grande taille (données RPLS, 5 millions de lignes, 115 colonnes, fichier de 250 Mo),  
    - Les scripts utilisent  la fonction system.time() pour mesurer la durée des étapes du traitement, lesquelles sont reprises dans la présentation,   
    - les données ne figurent pas dans ce présent projet, du fait de leur caractère confidentiel.  

**test.parquet_pop_sex_age_pluri_ann.R** :   
    exemple fonctionnel , comportant :  
    -  téléchargement d'un fichier XLSX depuis le site de l'INSEE,  
    - import multi-onglets et conversion en dataframe "tidy",  
    - conversion en formatParquet selon 3 modes : write_parquet(), write_dataset() et rds_to_parquet(),  
    - utilisation des données Parquet réimportées dans traitement avec {dplyr} et pipe `%>%`.

**pop_departementale.Rmd** et **pyramide_ages.Rmd** :  
    - 2 scripts d'exemple simples d'utilisation de données parquet.  
S'agissant de documents R Markdown, il est possible de les convertir en HTML.  
